# New Years Eve Hat

**Table of contents:**

[[_TOC_]]

## Purpose

Circuit with TM1637 and STM32F103C8T6 that will be glued to hat used for New Years Eve celebration. Its typical "improvised one day build".

## Simplified schematic

![Siplified schematic of cicruit](./Doc/Simpified_Schematic.png)

![Used pins of uC](./Doc/STM32F103C8Tx.png "Used pins of uC")

## List of components

- STM32F103C8T6 on [BluePill module](https://stm32-base.org/boards/STM32F103C8T6-Blue-Pill.html "BluePill module") PCB,
- [U1V10F5](https://www.pololu.com/product/2564 "U1V10F5") - step-up converter 5V 1,2A,
- TM1637 - 7 segment LED Dispaly,
- [LP883440](https://www.akyga.com/products/842-lithium-polymer-battery-3-7v-1200mah-pcm-connector-socket-2-54-jst-2-pins-l-150mm.html "LP883440") - Lithium Polymer Battery 3,7V/1200mAh, PCM,

## To Do

- Add on 5V line cappacitor (in case of battery short disconnection due to vibration)
- Add separate RTC battery
- Add possibility to set actual time and time setpoint via UART (or event classic bluetooth like HC05/HC06)

## Used software

- STM32CubeIDE v1.7.0,
- [STM32 TM1637 7-segment LED display library](https://github.com/rogerdahl/stm32-tm1637 "STM32 TM1637 7-segment LED display library") by [rogerdahl](https://github.com/rogerdahl "rogerdahl")
